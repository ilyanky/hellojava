package com.ilyanky.hellojava.lab3;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.Socket;
import java.security.interfaces.RSAPublicKey;

/**
 * Created by ilyanky on 5/19/15.
 */
public class Client
{
    private ObjectNode user = null;
    private JsonNodeFactory jsonNodeFactory = new JsonNodeFactory(false);
    private ObjectMapper objectMapper = new ObjectMapper();
    private String errorMsg;
    private Cipher encryptCipher;
    private Cipher decryptCipher;
    private SecretKeySpec key
            = new SecretKeySpec("abcerfsefdgrdasr".getBytes("UTF8"), "AES");


    public Client() throws Exception
    {
        encryptCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        decryptCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        encryptCipher.init(Cipher.ENCRYPT_MODE, key);
        decryptCipher.init(Cipher.DECRYPT_MODE, key);
    }


    private ObjectNode getObject(RequestType _type)
    {
        ObjectNode objectNode = jsonNodeFactory.objectNode();
        objectNode.putPOJO("requestType", _type);
        return objectNode;
    }


    private ObjectNode sendRequest(ObjectNode _node) throws Exception
    {
        receiveKey();

        Socket socket = new Socket("localhost", 2323);
        try ( ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
              ObjectInputStream ois = new ObjectInputStream(socket.getInputStream()) )
        {
            byte[] bytes = objectMapper.writeValueAsBytes(_node);
            bytes = encryptCipher.doFinal(bytes);
            oos.writeInt(bytes.length);
            oos.write(bytes);
            oos.flush();


            bytes = new byte[ois.readInt()];
            ois.readFully(bytes);
            bytes = decryptCipher.doFinal(bytes);
            JsonNode node = objectMapper.readTree(bytes);
            return (ObjectNode) node;
        }
    }



    /*
        Sign Up
     */
    public boolean signUp(String _login, String _pswd) throws Exception
    {
        ObjectNode signUpObject = getObject(RequestType.SIGN_UP);
        signUpObject.put("login", _login);
        signUpObject.put("password", _pswd);
        signUpObject = sendRequest(signUpObject);

        if ( !signUpObject.get("isSuccessful").asBoolean() ) {
            errorMsg = "some error in signUp";
            return false;
        }

        if ( signUpObject.get("containsUser").asBoolean() ) {
            errorMsg ="User with same login is already exist";
            return false;
        }

        return true;
    }


    /*
        Log In
    */
    public boolean logIn(String _login, String _pswd) throws Exception
    {
        ObjectNode logInObject = getObject(RequestType.LOG_IN);
        logInObject.put("login", _login);
        logInObject.put("password", _pswd);
        logInObject = sendRequest(logInObject);

        if ( !logInObject.get("isSuccessful").asBoolean() ) {
            errorMsg = "some error in logIn";
            return false;
        }

        if ( !logInObject.get("containsUser").asBoolean() ) {
            errorMsg = "There is no such user";
            return false;
        }

        if ( logInObject.get("wrongPassword").asBoolean() ) {
            errorMsg = "Wrong password or login";
            return false;
        }


        user = (ObjectNode) logInObject.get("user");

        return true;
    }


    /*
        Add Friend
    */
    public boolean addFriend(String _flogin) throws Exception
    {
        ObjectNode addFriendObject = getObject(RequestType.ADD_FRIEND);
        addFriendObject.put("login", getLogin());
        addFriendObject.put("friendLogin", _flogin);
        addFriendObject = sendRequest(addFriendObject);

        if ( !addFriendObject.get("isSuccessful").asBoolean() ) {
            errorMsg = "some error in addFriend";
            return false;
        }

        if ( !addFriendObject.get("containsUser").asBoolean() ) {
            errorMsg = "There is no such user";
            return false;
        }



        user = (ObjectNode) addFriendObject.get("user");

        return true;
    }


    /*
        Update User
    */
    public boolean updateUser() throws Exception
    {
        ObjectNode updateUserObject = getObject(RequestType.UPDATE_USER);
        updateUserObject.put("login", getLogin());
        updateUserObject = sendRequest(updateUserObject);

        if ( !updateUserObject.get("isSuccessful").asBoolean() ) {
            errorMsg = "some error in addFriend";
            return false;
        }


        user = (ObjectNode) updateUserObject.get("user");

        return true;
    }


    /*
        Send Message
    */
    public boolean sendMessage(String _flogin, String _text) throws Exception
    {
        ObjectNode sendMsgObject = getObject(RequestType.SEND_MSG);
        Message msg = new Message(getLogin(), _flogin, _text);
        sendMsgObject.putPOJO("message", msg);
        sendMsgObject.set("user", user);
        sendMsgObject = sendRequest(sendMsgObject);

        if ( !sendMsgObject.get("isSuccessful").asBoolean() ) {
            errorMsg = "some error in sendMessage";
            return false;
        }


        user = (ObjectNode) sendMsgObject.get("user");

        return true;
    }




    public boolean userLoaded()
    {
        return user != null;
    }


    public ObjectNode getUser()
    {
        return user;
    }


    public String getLogin()
    {
        return user.get("login").asText();
    }


    public String[] getFriends()
    {
        ArrayNode arrNode = (ArrayNode) user.get("friends");
        String[] friends = new String[arrNode.size()];
        for ( int i = 0; i < friends.length; ++i )
            friends[i] = arrNode.get(i).asText();

        return friends;
    }


    public String getErrorMsg()
    {
        return errorMsg;
    }


    public String getMsgHistory(String _flogin) throws IOException
    {
        String res = "";

        ArrayNode msgHistory = (ArrayNode) user.get("messageHistory");
        for ( int i = 0; i < msgHistory.size(); ++i ) {
            ObjectNode obj = (ObjectNode) msgHistory.get(i);
            if ( obj.get("login").asText().equals(_flogin) ) {
                ArrayNode messages = (ArrayNode) obj.get("values");
                for ( int j = 0; j < messages.size(); ++j ) {
                    Message msg = objectMapper.reader().forType(Message.class).readValue(messages.get(j));
                    res = res.concat(msg.getMessage()).concat("\n");
                }
            }
        }

        return res;
    }


    public void receiveKey() throws Exception
    {
        RSAPublicKey serverKey;
        try ( Socket socket = new Socket("localhost", 2323) ) {
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            serverKey = (RSAPublicKey) ois.readObject();
        }

        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.WRAP_MODE, serverKey);
        byte[] bkey = cipher.wrap(key);
        System.out.println(bkey.length);

        try ( Socket socket = new Socket("localhost", 2323) ) {
            ObjectOutputStream oos =
                    new ObjectOutputStream(socket.getOutputStream());
            oos.writeInt(bkey.length);
            oos.write(bkey);
            oos.flush();
        }
    }
}
