package com.ilyanky.hellojava.lab3;

import java.io.Serializable;

/**
 * Created by ilyanky on 17.05.2015.
 */
public class Message implements Serializable
{
    private String from;
    private String to;
    private String message;


    public Message()
    {
    }


    public Message(String from, String to, String message)
    {
        this.from = from;
        this.to = to;
        this.message = message;
    }


    public String getLoginFrom() {
        return from;
    }


    public void setLoginFrom(String from) {
        this.from = from;
    }


    public String getMessage() {
        return message;
    }


    public void setMessage(String message) {
        this.message = message;
    }


    public String getLoginTo() {
        return to;
    }


    public void setLoginTo(String to) {
        this.to = to;
    }


    @Override
    public String toString()
    {
        return "Message{" +
                "from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
