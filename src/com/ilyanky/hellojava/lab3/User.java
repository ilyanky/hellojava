package com.ilyanky.hellojava.lab3;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.Serializable;
import java.util.*;

/**
 * Created by ilyanky on 5/19/15.
 */
public class User implements Serializable
{
    private String nickname;
    private String password;
    private String login;
    private ArrayList<User> friends = new ArrayList<>();
    private HashMap<User, ArrayList<Message>> messageHistory
            = new HashMap<>();

    private ObjectMapper objectMapper = new ObjectMapper();


    public User()
    {

    }


    public User(String _login)
    {
        this.login = _login;
    }


    public User(String _nickname, String _password)
    {
        this.nickname = _nickname;
        this.password = _password;
    }


    public String getNickname()
    {
        return nickname;
    }


    public String getPassword()
    {
        return password;
    }


    public ArrayList<User> getFriends()
    {
        return friends;
    }


    public void addFriend(User _user)
    {
        friends.add(_user);
        messageHistory.put(_user, new ArrayList<>());
    }


    public String getLogin()
    {
        return login;
    }


    public void setLogin(String login)
    {
        this.login = login;
    }


    public ObjectNode getInfo()
    {
        ObjectNode result = objectMapper.createObjectNode();
        result.put("nickname", nickname);
        result.put("login", login);

        ArrayNode friendsArr = result.putArray("friends");
        for ( User friend : friends )
            friendsArr.add(friend.getLogin());

        ArrayNode msgHistory = result.putArray("messageHistory");
        for ( User u : messageHistory.keySet() ) {
            ObjectNode obj = objectMapper.createObjectNode();
            obj.put("login", u.getLogin());
            ArrayNode arrayNode = obj.putArray("values");
            messageHistory.get(u).forEach(arrayNode::addPOJO);
            msgHistory.add(obj);
        }

        return result;
    }


    public ArrayList<Message> getMessageHistory(String _flogin)
    {
        Optional<User> user = friends.stream().filter(
                (_friend) -> _friend.getLogin().equals(_flogin)
        ).findFirst();
        return messageHistory.get(user.get());
    }


    public void addMessage(String _loginTo, Message _msg)
    {
        Optional<User> user = friends.stream().filter(
                (_friend) -> _friend.getLogin().equals(_loginTo)
        ).findFirst();
        if ( user.isPresent() )
            messageHistory.get(user.get()).add(_msg);
        else {
            friends.add(new User(_loginTo));
        }
    }


    public boolean isFriend(String _login)
    {
        Optional<User> user = friends.stream().filter(x -> x.getLogin().equals(_login)).findFirst();
        return user.isPresent();
    }
}
