package com.ilyanky.hellojava.lab3.dialogs;

import javax.swing.*;
import java.awt.*;

/**
 * Created by ilyanky on 5/21/15.
 */
public class LogInDialog extends JDialog
{
    private JTextField loginField = null;
    private JPasswordField pswdField = null;
    private boolean ok = false;


    public LogInDialog(JFrame owner)
    {
        super(owner, "Log In", true);

        loginField = new JTextField();
        pswdField = new JPasswordField();

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(2, 2));
        panel.add(new JLabel("login: "));
        panel.add(loginField);
        panel.add(new JLabel("Password: "));
        panel.add(pswdField);
        this.add(panel, BorderLayout.CENTER);

        JButton signUpButton = new JButton("Log In");
        signUpButton.addActionListener((_action) -> {
            if ( loginField.getText().isEmpty() ) {
                JOptionPane.showMessageDialog(LogInDialog.this, "Empty login field");
                ok = false;
            }

            if ( pswdField.getPassword().length < 4 ) {
                JOptionPane.showMessageDialog(LogInDialog.this, "Too short password");
                ok = false;
            }

            ok = true;
            this.setVisible(false);
        });

        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener((_action) -> {
            ok = false;
            this.setVisible(false);
        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(signUpButton);
        buttonPanel.add(cancelButton);
        this.add(buttonPanel, BorderLayout.SOUTH);

        this.pack();
    }


    public boolean showDialog()
    {
        this.setVisible(true);
        return ok;
    }


    public String getLogin()
    {
        return loginField.getText();
    }


    public String getPassword()
    {
        return new String(pswdField.getPassword());
    }
}
