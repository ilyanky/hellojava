package com.ilyanky.hellojava.lab3.dialogs;

import javax.swing.*;
import java.awt.*;

/**
 * Created by ilyanky on 5/22/15.
 */
public class AddFriendDialog extends JDialog
{
    private JTextField loginField = null;
    private boolean ok = false;


    public AddFriendDialog(JFrame owner)
    {
        super(owner, "Add Friend", true);

        loginField = new JTextField();

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(1, 2));
        panel.add(new JLabel("Login: "));
        panel.add(loginField);
        this.add(panel, BorderLayout.CENTER);

        JButton findButton = new JButton("Find");
        findButton.addActionListener((_action) -> {
            if ( loginField.getText().isEmpty() ) {
                JOptionPane.showMessageDialog(AddFriendDialog.this, "Empty login field");
                ok = false;
            }

            ok = true;
            this.setVisible(false);
        });

        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener((_action) -> {
            ok = false;
            this.setVisible(false);
        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(findButton);
        buttonPanel.add(cancelButton);
        this.add(buttonPanel, BorderLayout.SOUTH);

        this.pack();
    }


    public boolean showDialog()
    {
        this.setVisible(true);
        return ok;
    }


    public String getLogin()
    {
        return loginField.getText();
    }
}
