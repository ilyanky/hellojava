package com.ilyanky.hellojava.lab3;

/**
 * Created by ilyanky on 5/19/15.
 */
public enum RequestType
{
    LOG_IN,
    SEND_MSG,
    SIGN_UP,
    ADD_FRIEND,
    UPDATE_USER
}
