package com.ilyanky.hellojava.lab3;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.*;
import java.util.HashMap;
import java.util.function.Function;

/**
 * Created by ilyanky on 5/21/15.
 */
public class Requests
{
    private static ObjectNode getObject()
    {
        return new ObjectMapper().createObjectNode();
    }


    /*
      Sign Up
     */
    static Function<JsonNode, ObjectNode> getSignUpFunction()
    {
        return jsonNode -> {

            HashMap<String, User> users = null;

            ObjectNode errorNode = getObject();
            errorNode.put("isSuccessful", false);


            try ( ObjectInputStream ois = new ObjectInputStream(
                    new FileInputStream("./src/com/ilyanky/hellojava/lab3/users")) )
            {
                users = (HashMap<String, User>) ois.readObject();
            } catch ( IOException | ClassNotFoundException e ) {
                e.printStackTrace();
                return errorNode;
            }


            ObjectNode objectNode = getObject();
            String login = jsonNode.get("login").asText();
            if ( users.containsKey(login) ) {
                objectNode.put("isSuccessful", true);
                objectNode.put("containsUser", true);
                return objectNode;
            } else try ( ObjectOutputStream oos = new ObjectOutputStream(
                    new FileOutputStream("./src/com/ilyanky/hellojava/lab3/users")) )
            {
                User user = new User(login, jsonNode.get("password").asText());
                user.setLogin(login);
                users.put(login, user);
                oos.writeObject(users);

                objectNode.put("isSuccessful", true);
                objectNode.put("containsUser", false);
                return objectNode;
            } catch ( IOException e ) {
                e.printStackTrace();
                return errorNode;
            }


        };
    }


    /*
      Log In
     */
    static Function<JsonNode, ObjectNode> getLogInFunction()
    {
        return jsonNode -> {

            HashMap<String, User> users = null;

            ObjectNode errorNode = getObject();
            errorNode.put("isSuccessful", false);


            try ( ObjectInputStream ois = new ObjectInputStream(
                    new FileInputStream("./src/com/ilyanky/hellojava/lab3/users")) )
            {
                users = (HashMap<String, User>) ois.readObject();
            } catch ( IOException | ClassNotFoundException e ) {
                e.printStackTrace();
                return errorNode;
            }


            ObjectNode objectNode = getObject();
            objectNode.put("isSuccessful", true);
            objectNode.put("containsUser", true);

            String login = jsonNode.get("login").asText();
            if ( users.containsKey(login) ) {
                if ( !users.get(login).getPassword().equals(jsonNode.get("password").asText()) )
                {
                    objectNode.put("wrongPassword", true);
                    return objectNode;
                } else {
                    objectNode.put("wrongPassword", false);
                    objectNode.set("user", users.get(login).getInfo());
                    return objectNode;
                }
            } else {
                objectNode.put("containsUser", false);
                return objectNode;
            }

        };
    }


    /*
        Add Friend
    */
    static Function<JsonNode, ObjectNode> getAddFriendFunction()
    {
        return jsonNode -> {

            HashMap<String, User> users = null;

            ObjectNode errorNode = getObject();
            errorNode.put("isSuccessful", false);


            try ( ObjectInputStream ois = new ObjectInputStream(
                    new FileInputStream("./src/com/ilyanky/hellojava/lab3/users")) )
            {
                users = (HashMap<String, User>) ois.readObject();
            } catch ( IOException | ClassNotFoundException e ) {
                e.printStackTrace();
                return errorNode;
            }


            ObjectNode objectNode = getObject();
            objectNode.put("isSuccessful", true);
            objectNode.put("containsUser", true);

            String flogin = jsonNode.get("friendLogin").asText();
            String login = jsonNode.get("login").asText();
            if ( users.containsKey(flogin) )
            {
                users.get(login).addFriend(users.get(flogin));
                try ( ObjectOutputStream oos = new ObjectOutputStream(
                        new FileOutputStream("./src/com/ilyanky/hellojava/lab3/users")) )
                {
                    oos.writeObject(users);
                } catch ( IOException e ) {
                    e.printStackTrace();
                    return errorNode;
                }

                objectNode.set("user", users.get(login).getInfo());
                return objectNode;
            } else {
                objectNode.put("containsUser", false);
                return objectNode;
            }

        };
    }


    /*
        Update User
    */
    static Function<JsonNode, ObjectNode> getUpdateUserFunction()
    {
        return jsonNode -> {

            ObjectMapper objectMapper = new ObjectMapper();

            HashMap<String, User> users = null;

            ObjectNode errorNode = getObject();
            errorNode.put("isSuccessful", false);


            try ( ObjectInputStream ois = new ObjectInputStream(
                    new FileInputStream("./src/com/ilyanky/hellojava/lab3/users")) )
            {
                users = (HashMap<String, User>) ois.readObject();
            } catch ( IOException | ClassNotFoundException e ) {
                e.printStackTrace();
                return errorNode;
            }


            ObjectNode objectNode = objectMapper.createObjectNode();
            objectNode.put("isSuccessful", true);
            objectNode.set("user", users.get(jsonNode.get("login").asText()).getInfo());
            return objectNode;

        };
    }


    /*
        Send Message
    */
    static Function<JsonNode, ObjectNode> getSendMsgFunction()
    {
        return jsonNode -> {

            ObjectMapper objectMapper = new ObjectMapper();

            HashMap<String, User> users = null;

            ObjectNode errorNode = getObject();
            errorNode.put("isSuccessful", false);


            try ( ObjectInputStream ois = new ObjectInputStream(
                    new FileInputStream("./src/com/ilyanky/hellojava/lab3/users")) )
            {
                users = (HashMap<String, User>) ois.readObject();
            } catch ( IOException | ClassNotFoundException e ) {
                e.printStackTrace();
                return errorNode;
            }


            ObjectNode objectNode = getObject();
            objectNode.put("isSuccessful", true);

            ObjectReader reader = objectMapper.reader().forType(Message.class);
            Message msg;
            try {
                msg = reader.readValue(jsonNode.get("message"));
            } catch ( IOException e ) {
                e.printStackTrace();
                return errorNode;
            }


            users.get(msg.getLoginFrom()).addMessage(msg.getLoginTo(), msg);
            if ( !users.get(msg.getLoginTo()).isFriend(msg.getLoginFrom()) )
                users.get(msg.getLoginTo()).addFriend(users.get(msg.getLoginFrom()));

            users.get(msg.getLoginTo()).addMessage(msg.getLoginFrom(), msg);

            try ( ObjectOutputStream oos = new ObjectOutputStream(
                    new FileOutputStream("./src/com/ilyanky/hellojava/lab3/users")) )
            {
                oos.writeObject(users);
            } catch ( IOException e ) {
                e.printStackTrace();
                return errorNode;
            }


            objectNode.set("user", users.get(msg.getLoginFrom()).getInfo());
            return objectNode;

        };
    }
}
