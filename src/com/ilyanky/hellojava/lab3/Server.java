package com.ilyanky.hellojava.lab3;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.*;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.function.Function;

/**
 * Created by ilyanky on 17.05.2015.
 */
public class Server
{
    private ServerSocket server;
    private final HashMap<RequestType, Function<JsonNode, ObjectNode>> requestMap;
    private KeyPair serverKeyPair;
    private Cipher encryptCipher;
    private Cipher decryptCipher;


    public Server(int port) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException
    {
        requestMap = new HashMap<>();
        requestMap.put(RequestType.SIGN_UP, Requests.getSignUpFunction());
        requestMap.put(RequestType.LOG_IN, Requests.getLogInFunction());
        requestMap.put(RequestType.ADD_FRIEND, Requests.getAddFriendFunction());
        requestMap.put(RequestType.SEND_MSG, Requests.getSendMsgFunction());
        requestMap.put(RequestType.UPDATE_USER, Requests.getUpdateUserFunction());


        encryptCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        decryptCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");

        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(1024);
        serverKeyPair = kpg.generateKeyPair();


        this.server = new ServerSocket(port);


        File file = new File("./src/com/ilyanky/hellojava/lab3/users");
        if ( file.createNewFile() ) {
            try ( ObjectOutputStream oos = new ObjectOutputStream(
                    new FileOutputStream("./src/com/ilyanky/hellojava/lab3/users")) )
            {
                oos.writeObject(new HashMap<String, User>());
            }
        }
    }


    private byte[] acceptRequest(byte[] _request) throws IOException
    {
        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode node = objectMapper.readTree(_request);
        ObjectReader reader = objectMapper.reader().forType(RequestType.class);
        RequestType rt = reader.readValue(node.get("requestType"));
        synchronized (requestMap) {
            return objectMapper.writeValueAsBytes(requestMap.get(rt).apply(node));
        }
    }


    public void foo() throws Exception
    {
        ExecutorService tpp = Executors.newFixedThreadPool(4);
        Semaphore semaphore = new Semaphore(4);

        while ( true ) {

            semaphore.acquire();
            receiveKey();
            Socket newSocket = server.accept();
            tpp.execute(() -> {

                Socket socket = newSocket;
                System.out.println("client");
                try ( ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                      ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream()) )
                {
                    byte[] bytes = new byte[ois.readInt()];
                    ois.readFully(bytes);
                    bytes = decryptCipher.doFinal(bytes);

                    bytes = encryptCipher.doFinal(acceptRequest(bytes));
                    oos.writeInt(bytes.length);
                    oos.write(bytes);
                    oos.flush();
                } catch ( Exception e ) {
                    e.printStackTrace();
                } finally {
                    semaphore.release();
                }

            });

        }
    }


    public synchronized void receiveKey() throws Exception
    {
        try ( Socket socket = server.accept() ) {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(serverKeyPair.getPublic());
            oos.flush();
        }


        byte[] bkey;
        try ( Socket socket = server.accept() ) {
            ObjectInputStream ois =
                    new ObjectInputStream(socket.getInputStream());
            int len = ois.readInt();
            System.out.println(len);
            bkey = new byte[len];
            ois.read(bkey);
        }

        Cipher sdecryptCipher;
        sdecryptCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        sdecryptCipher.init(Cipher.UNWRAP_MODE, serverKeyPair.getPrivate());

        SecretKeySpec clientKey = (SecretKeySpec)
                sdecryptCipher.unwrap(bkey, "AES", Cipher.SECRET_KEY);

        decryptCipher.init(Cipher.DECRYPT_MODE, clientKey);
        encryptCipher.init(Cipher.ENCRYPT_MODE, clientKey);
    }


    public static void main(String[] args) throws Exception
    {
        Server server = new Server(2323);
        server.foo();
    }
}
