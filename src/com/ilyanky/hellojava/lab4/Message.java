package com.ilyanky.hellojava.lab4;

import java.io.Serializable;

/**
 * Created by ilyanky on 17.05.2015.
 */
public class Message implements Serializable
{
    private String from;
    private String to;
    private String text;


    public Message()
    {
    }


    public Message(String from, String to, String text)
    {
        this.from = from;
        this.to = to;
        this.text = text;
    }


    public String getLoginFrom() {
        return from;
    }


    public String getText() {
        return text;
    }


    public String getLoginTo() {
        return to;
    }


    @Override
    public String toString()
    {
        return "Message{" +
                "from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", text='" + text + '\'' +
                '}';
    }


    public static String toStringMsg(String from, String text)
    {
        return from + "\n\t" + text;
    }
}
