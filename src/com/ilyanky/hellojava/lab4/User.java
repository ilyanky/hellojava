package com.ilyanky.hellojava.lab4;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by ilyanky on 5/19/15.
 */
public class User implements Serializable
{
    private String password;
    private String login;
    private HashSet<String> friends = new HashSet<>();
    private HashMap<String, ArrayList<Message>> messageHistory
            = new HashMap<>();

    public User()
    {
    }


    public User(String login)
    {
        this.login = login;
    }


    public User(String login, String password)
    {
        this.login = login;
        this.password = password;
    }


    public User(ObjectNode node) throws JsonProcessingException
    {
        password = node.get("password").asText();
        login = node.get("login").asText();
        friends = new ObjectMapper().treeToValue(node.get("friends"), HashSet.class);
    }


    public HashSet<String> getFriends()
    {
        return friends;
    }


    public void addFriend(String login)
    {
        if ( friends.contains(login) )
            return;

        friends.add(login);
        messageHistory.put(login, new ArrayList<>());
    }


    public String getLogin()
    {
        return login;
    }


    public void setLogin(String login)
    {
        this.login = login;
    }


    public String getPassword()
    {
        return password;
    }


    public static synchronized HashMap<String, User>
            getUsers(String fileName)
    {
        try ( ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream(fileName)) )
        {
            HashMap m = (HashMap<String, User>)ois.readObject();
            System.out.println(m.toString());
            return m;
        } catch ( IOException | ClassNotFoundException e ) {
            e.printStackTrace();
            return null;
        }
    }


    public ObjectNode toObjectNode()
    {
        ObjectNode res = new ObjectMapper().createObjectNode();

        res.put("password", password);
        res.put("login", login);
        res.putPOJO("friends", friends);

        return res;
    }


    public void addMessage(String friendLogin, Message msg)
    {
        addFriend(friendLogin);
        messageHistory.get(friendLogin).add(msg);
    }


    @Override
    public String toString()
    {
        return "User{" +
                "password='" + password + '\'' +
                ", login='" + login + '\'' +
                ", friends=" + friends +
                ", messageHistory=" + messageHistory +
                '}';
    }

    public String toStringWithoutPswd()
    {
        return "User{" +
                "login='" + login + '\'' +
                ", friends=" + friends +
                '}';
    }
}
