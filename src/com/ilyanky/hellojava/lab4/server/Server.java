package com.ilyanky.hellojava.lab4.server;

import com.ilyanky.hellojava.lab4.*;
import com.ilyanky.hellojava.lab4.request.*;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

/**
 * Created by ilyanky on 17.05.2015.
 */
public class Server
{
    private int port = 2323;
    private String userFile = "./src/com/ilyanky/hellojava/lab4/users";
    private String msgFile = "./src/com/ilyanky/hellojava/lab4/msgArchive";

    private EventLoopGroup bossGroup;
    private EventLoopGroup workerGroup;
    private ServerBootstrap bootstrap;
    private ConcurrentHashMap<String, Channel> clients =
            new ConcurrentHashMap<>();



    private HashMap<RequestType, Function<Request, Request>> requestMap
            = new HashMap<>();

    private ConcurrentHashMap<String, User> users;



    public Server() throws Exception
    {
        requestMap.put(RequestType.LOG_IN, getLogInFunction());
        requestMap.put(RequestType.SIGN_UP, getSignUpFunction());
        requestMap.put(RequestType.ADD_FRIEND, getAddFriendFunction());
        requestMap.put(RequestType.SEND_MSG, getSendMsgFunction());

        initServer();
        initFiles();

        users = new ConcurrentHashMap<>(User.getUsers(userFile));
    }


    private void initServer()
    {
        bossGroup = new NioEventLoopGroup();
        workerGroup = new NioEventLoopGroup();

        bootstrap = new ServerBootstrap();
        bootstrap.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .option(ChannelOption.SO_BACKLOG, 128)
                .childOption(ChannelOption.SO_KEEPALIVE, true)
                .childHandler(new ChannelInitializer<SocketChannel>()
                {
                    @Override
                    public void initChannel(SocketChannel ch) throws Exception
                    {
                        ch.pipeline().addLast(new RequestDecoder(), new RequestEncoder(),
                                new ServerHandler(requestMap, clients));
                    }
                });
    }


    private void initFiles() throws Exception
    {
        File file = new File(userFile);
        if ( file.createNewFile() ) {
            try ( ObjectOutputStream oos = new ObjectOutputStream(
                    new FileOutputStream(userFile)) )
            {
                oos.writeObject(new HashMap<String, User>());
            }
        }

        file = new File(msgFile);
        if ( file.createNewFile() ) {
            try ( ObjectOutputStream oos = new ObjectOutputStream(
                    new FileOutputStream(msgFile)) )
            {
                oos.writeObject(new HashMap<String, ArrayList<String>>());
            }
        }
    }


    public void run() throws Exception
    {
        try {
            ChannelFuture cf = bootstrap.bind(port).sync();
            cf.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }



    /**
     * Log In function
     *
     * @return
     */
    private Function<Request, Request> getLogInFunction()
    {
        return req -> {
            LogInRequest request = (LogInRequest)req;

            LogInRequest result = new LogInRequest();
            result.setSuccessful(true);

            String login = request.getLogin();
            User user = users.get(login);
            if ( user == null ) {
                result.setUserContains(false);
                result.setSuccessful(true);
                return result;
            }

            if ( user.getPassword().equals(request.getPassword()) ) {
                result = new LogInRequest(user.toObjectNode());
                result.setSuccessful(true);
            } else {
                result.setUserContains(true);
                result.setAuthError(true);
            }

            return result;
        };
    }


    /**
     * Sign Up function
     *
     * @return
     */
    private Function<Request, Request> getSignUpFunction()
    {
        return req -> {
            SignUpRequest request = (SignUpRequest)req;

            SignUpRequest result = new SignUpRequest();
            result.setSuccessful(true);

            System.out.println(users.toString());

            String login = request.getLogin();
            User user = users.get(login);
            if ( user == null ) {
                users.put(request.getLogin(), new User(request.getLogin(), request.getPassword()));
                result.setUserContains(false);
            }
            else
                result.setUserContains(true);

            return result;
        };
    }



    /**
     * Add friend function
     *
     * @return
     */
    private Function<Request, Request> getAddFriendFunction()
    {
        return req -> {
            AddFriendRequest request = (AddFriendRequest)req;

            AddFriendRequest result = new AddFriendRequest();
            result.setSuccessful(true);

            String friendLogin = request.getFriendLogin();
            String login = request.getLogin();
            User friend = users.get(friendLogin);
            if ( friend != null) {
                friend.addFriend(login);
                users.get(login).addFriend(friendLogin);
                result.setUserContains(true);
            }
            else
                result.setUserContains(false);

            return result;
        };
    }



    /**
     * Send message function
     *
     * @return
     */
    private Function<Request, Request> getSendMsgFunction()
    {
        return req -> {
            SendMsgRequest request = (SendMsgRequest)req;

            SendMsgRequest result = new SendMsgRequest();
            result.setSuccessful(true);

            try {
                Message msg = request.getMessage();
                String fromLogin = msg.getLoginFrom();
                String toLogin = msg.getLoginTo();

                users.get(fromLogin).addMessage(toLogin, msg);
                users.get(toLogin).addMessage(fromLogin, msg);
                System.out.println("from getSendMsg\n" + clients.toString());
                System.out.println(clients.get(toLogin));
                clients.get(toLogin).writeAndFlush(new MsgReceived(msg));

                return result;
            } catch (IOException e) {
                e.printStackTrace();
                result.setSuccessful(false);
                return result;
            }
        };
    }



    public static void main(String[] args) throws Exception
    {
        new Server().run();
    }
}
