package com.ilyanky.hellojava.lab4.server;

import com.ilyanky.hellojava.lab4.RequestType;
import com.ilyanky.hellojava.lab4.request.LogInRequest;
import com.ilyanky.hellojava.lab4.request.Request;
import io.netty.channel.*;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;


/**
 * Created by ilyanky on 6/3/15.
 */
public class ServerHandler extends SimpleChannelInboundHandler<Request>
{
    private HashMap<RequestType, Function<Request, Request>> map;
    private ConcurrentHashMap<String, Channel> clients =
            new ConcurrentHashMap<>();


    public ServerHandler(HashMap<RequestType, Function<Request, Request>> m,
                         ConcurrentHashMap<String, Channel> c)
    {
        map = m;
        clients = c;
    }


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Request msg) throws Exception
    {
        Request answer = map.get(msg.getRequestType()).apply(msg);
        if ( msg.getRequestType() == RequestType.LOG_IN ) {
            if ( answer.isSuccessful() ) {
                LogInRequest r = (LogInRequest)msg;
                clients.put(r.getLogin(), ctx.channel());
                System.out.println(clients.toString());
            }
        }

        ctx.writeAndFlush(answer);
        System.out.println("read");
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
    {
        cause.printStackTrace();
        ctx.close();
    }
}