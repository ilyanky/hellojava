package com.ilyanky.hellojava.lab4;

import com.ilyanky.hellojava.lab4.request.Request;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.io.IOException;
import java.util.List;


/**
 * Created by ilyanky on 6/3/15.
 */
public class RequestDecoder extends ByteToMessageDecoder
{
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws IOException
    {
        if ( in.readableBytes() < 4 )
            return;

        in.markReaderIndex();

        int dataLength = in.readInt();
        if ( in.readableBytes() < dataLength ) {
            in.resetReaderIndex();
            return;
        }


        int id = in.readInt();
        byte[] decoded = new byte[dataLength - 4];
        in.readBytes(decoded);

        out.add(Request.getRequest(id, decoded));
    }
}
