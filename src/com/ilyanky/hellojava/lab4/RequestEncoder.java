package com.ilyanky.hellojava.lab4;

import com.ilyanky.hellojava.lab4.request.Request;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * Created by ilyanky on 6/3/15.
 */
public class RequestEncoder extends MessageToByteEncoder
{
    @Override
    protected void encode(ChannelHandlerContext ctx, Object msg, ByteBuf out) throws Exception
    {
        Request r = (Request) msg;
        byte[] bytes = r.toBytes();

        out.writeInt(bytes.length);
        out.writeBytes(bytes);
    }
}