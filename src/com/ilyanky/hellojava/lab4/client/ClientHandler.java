package com.ilyanky.hellojava.lab4.client;

import com.ilyanky.hellojava.lab4.request.MsgReceived;
import com.ilyanky.hellojava.lab4.request.Request;
import io.netty.channel.*;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;

/**
 * Created by ilyanky on 6/3/15.
 */
public class ClientHandler extends SimpleChannelInboundHandler<Request>
{
    private LinkedBlockingQueue<Request> requests = new LinkedBlockingQueue<>();
    private Consumer<Request> consumer;

    ClientHandler( Consumer<Request> c )
    {
        consumer = c;
    }


    public Request getLastRequest() throws InterruptedException
    {
        return requests.take();
    }


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Request msg) throws Exception
    {
        if ( msg instanceof MsgReceived )
            consumer.accept(msg);
        else
            requests.offer(msg);
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
    {
        cause.printStackTrace();
        ctx.close();
    }
}
