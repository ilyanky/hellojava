package com.ilyanky.hellojava.lab4.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ilyanky.hellojava.lab4.Message;
import com.ilyanky.hellojava.lab4.RequestDecoder;
import com.ilyanky.hellojava.lab4.RequestEncoder;
import com.ilyanky.hellojava.lab4.User;
import com.ilyanky.hellojava.lab4.request.*;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.io.IOException;
import java.util.HashSet;
import java.util.Observable;

/**
 * Created by ilyanky on 5/19/15.
 */
public class Client extends Observable
{
    private static String host = "localhost";
    private static int port = 2323;

    private EventLoopGroup workerGroup;
    private ChannelFuture cf;

    private String errorMsg;


    private User currUser = null;


    public Client() throws Exception
    {
        ClientHandler clientHandler = new ClientHandler((req) -> {
            messageReceived((MsgReceived)req);
        });

        workerGroup = new NioEventLoopGroup();
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(workerGroup);
        bootstrap.channel(NioSocketChannel.class);
        bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
        bootstrap.handler(new ChannelInitializer<SocketChannel>()
        {
            @Override
            public void initChannel(SocketChannel ch) throws Exception
            {
                ch.pipeline().addLast(new RequestDecoder(), new RequestEncoder(), clientHandler);
            }
        });

        cf = bootstrap.connect(host, port).sync();
    }


    public void close() throws InterruptedException
    {
        workerGroup.shutdownGracefully();
        cf.channel().closeFuture().sync();
    }


    private ClientHandler sendRequest(Request r)
    {
        ChannelFuture f = cf.channel().writeAndFlush(r);
        return (ClientHandler)f.channel().pipeline().last();
    }




    /*
        Sign Up
     */
    public boolean signUp(String login, String pswd)
    {
        try
        {
            SignUpRequest request = (SignUpRequest)
                    sendRequest(new SignUpRequest(login, pswd)).getLastRequest();

            if (!request.isSuccessful()) {
                errorMsg = "some error in signUp";
                return false;
            }
            if (request.containsUser()) {
                errorMsg = "User with same login is already exist";
                return false;
            }

            return true;
        } catch (InterruptedException e) {
            e.printStackTrace();
            errorMsg = "some error in signUp";
            return false;
        }
    }


    /*
        Log In
    */
    public boolean logIn(String login, String pswd)
    {
        try
        {
            LogInRequest request = (LogInRequest)
                    sendRequest(new LogInRequest(login, pswd)).getLastRequest();

            if (!request.isSuccessful()) {
                errorMsg = "some error in logIn";
                return false;
            }
            if (!request.containsUser()) {
                errorMsg = "There is no such user";
                return false;
            }
            if (request.authError()) {
                errorMsg = "Wrong password or login";
                return false;
            }

            currUser = request.getUser();

            return true;
        } catch (InterruptedException | JsonProcessingException e) {
            e.printStackTrace();
            errorMsg = "some error in logIn";
            return false;
        }
    }


    /*
        Add Friend
    */
    public boolean addFriend(String friendLogin)
    {
        try {
            AddFriendRequest request = (AddFriendRequest)sendRequest(
                    new AddFriendRequest(currUser.getLogin(), friendLogin)
            ).getLastRequest();


            if ( !request.isSuccessful() ) {
                errorMsg = "some error in addFriend";
                return false;
            }
            if ( !request.containsUser() ) {
                errorMsg = "There is no such user";
                return false;
            }

            currUser.addFriend(friendLogin);

            return true;
        } catch (InterruptedException e) {
            e.printStackTrace();
            errorMsg = "some error in addFriend";
            return false;
        }
    }


    /*
        Send Message
    */
    public boolean sendMessage(String friendLogin, String text)
    {
        try {
            SendMsgRequest request = (SendMsgRequest)sendRequest(
                    new SendMsgRequest(
                            new Message(currUser.getLogin(), friendLogin, text)
                    )
            ).getLastRequest();

            if ( !request.isSuccessful() ) {
                errorMsg = "some error in sendMessage";
                return false;
            }

            currUser.addMessage(friendLogin, new Message(currUser.getLogin(), friendLogin, text));

            return true;
        } catch (InterruptedException e) {
            e.printStackTrace();
            errorMsg = "some error in sendMessage";
            return false;
        }
    }


    private void messageReceived(MsgReceived mr)
    {
        try {
            Message m = mr.getMessage();
            currUser.addMessage(m.getLoginFrom(), m);
            setChanged();
            notifyObservers(m);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(currUser.toString());
    }




    public String[] getFriends()
    {
        HashSet<String> friends = currUser.getFriends();
        return friends.toArray(new String[friends.size()]);
    }

    public String getErrorMsg()
    {
        return errorMsg;
    }

    public String getUserInfo()
    {
        return currUser.toStringWithoutPswd();
    }

    public boolean userLoaded()
    {
        return currUser != null;
    }

    public String getUserLogin()
    {
        return currUser.getLogin();
    }
}
