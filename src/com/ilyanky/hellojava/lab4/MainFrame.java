package com.ilyanky.hellojava.lab4;

import com.ilyanky.hellojava.lab4.client.Client;
import com.ilyanky.hellojava.lab4.dialogs.AddFriendDialog;
import com.ilyanky.hellojava.lab4.dialogs.InfoDialog;
import com.ilyanky.hellojava.lab4.dialogs.LogInDialog;
import com.ilyanky.hellojava.lab4.dialogs.SignUpDialog;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by ilyanky on 5/21/15.
 */
public class MainFrame extends JFrame implements Observer
{
    private SignUpDialog signUpDialog = null;
    private LogInDialog logInDialog = null;
    private AddFriendDialog addFriendDialog = null;
    private InfoDialog infoDialog = null;

    private final JTextArea textArea = new JTextArea(30, 80);
    private JTextArea messageArea = new JTextArea(10, 80);

    private JList<String> friendList = null;

    private Client client;


    public MainFrame() throws Exception
    {
        client = new Client();
        client.addObserver(this);


        this.setJMenuBar(createMenuBar());


        friendList = new JList<>(new DefaultListModel<>());
        friendList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane listScrollPane = new JScrollPane(friendList);


        textArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(textArea);


        JScrollPane messageScrollPane = new JScrollPane(messageArea);


        JButton sendButton = new JButton("Send");
        sendButton.addActionListener((_action) -> {
            String msg;
            if ( client.sendMessage(friendList.getSelectedValue(), messageArea.getText()) )
                msg = "Message delivered";
            else
                msg = client.getErrorMsg();

            textArea.append("\n" + Message.toStringMsg(client.getUserLogin(), messageArea.getText()));
            JOptionPane.showMessageDialog(MainFrame.this, msg);
        });


        GroupLayout layout = new GroupLayout(this.getContentPane());
        this.getContentPane().setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup().addComponent(listScrollPane).addComponent(scrollPane))
                        .addComponent(messageScrollPane)
                        .addComponent(sendButton)
        );

        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(listScrollPane)
                        .addComponent(scrollPane))
                .addComponent(messageScrollPane)
                .addComponent(sendButton)
        );


        this.pack();
    }






    private JMenuBar createMenuBar()
    {
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");

        fileMenu.add(createLogInItem());
        fileMenu.add(createSignUpItem());
        fileMenu.add(createUserInfoItem());
        fileMenu.add(createAddFriendItem());
        menuBar.add(fileMenu);

        return menuBar;
    }


    private JMenuItem createSignUpItem()
    {
        JMenuItem signUpItem = new JMenuItem("Sign Up");
        signUpItem.addActionListener((_action) -> {
            if (signUpDialog == null) signUpDialog = new SignUpDialog(MainFrame.this);

            if (signUpDialog.showDialog()) {
                String msg;
                if (client.signUp(signUpDialog.getlogin(), signUpDialog.getPassword()))
                    msg = "User successfully signed up";
                else msg = client.getErrorMsg();
                JOptionPane.showMessageDialog(MainFrame.this, msg);
            }
        });

        return signUpItem;
    }



    private JMenuItem createLogInItem()
    {
        JMenuItem logInItem = new JMenuItem("Log in");
        logInItem.addActionListener( (_action) ->
        {
            if (logInDialog == null) logInDialog = new LogInDialog(MainFrame.this);

            if (logInDialog.showDialog()) {
                String msg;
                if ( client.logIn(logInDialog.getLogin(), logInDialog.getPassword()) ) {
                        friendList.setListData(client.getFriends());
                    msg = "You successfully logged in";
                } else msg = client.getErrorMsg();
                JOptionPane.showMessageDialog(MainFrame.this, msg);
            }
        });

        return logInItem;
    }


    private JMenuItem createUserInfoItem()
    {
        JMenuItem logInItem = new JMenuItem("Info");
        logInItem.addActionListener( (_action) ->
        {
            if ( infoDialog == null )
                infoDialog = new InfoDialog(MainFrame.this);
            infoDialog.setInfo(client.getUserInfo());
            infoDialog.setVisible(true);
        });

        return logInItem;
    }


    private JMenuItem createAddFriendItem()
    {
        JMenuItem addFriendItem = new JMenuItem("Add friend");
        addFriendItem.addActionListener( (_action) ->
        {
            if ( !client.userLoaded() ) {
                JOptionPane.showMessageDialog(MainFrame.this, "Log in first");
                return;
            }
            if ( addFriendDialog == null )
                addFriendDialog = new AddFriendDialog(MainFrame.this);

            if ( addFriendDialog.showDialog() )
            {
                String msg;
                if ( client.addFriend(addFriendDialog.getLogin()) ) {
                    friendList.setListData(client.getFriends());
                    msg = "You successfully added a friend";
                }
                else
                    msg = client.getErrorMsg();
                JOptionPane.showMessageDialog(MainFrame.this, msg);
            }
        });

        return addFriendItem;
    }


    @Override
    public void update(Observable o, Object arg)
    {
        Message m = (Message)arg;

        if ( friendList.getSelectedValue().equals(m.getLoginFrom()) )
            textArea.append("\n" + Message.toStringMsg(m.getLoginFrom(), m.getText()));
    }




    public static void main(String[] args)
    {
        EventQueue.invokeLater(() -> {
            MainFrame frame = null;

            try {
                frame = new MainFrame();
            } catch ( Exception  e ) {
                e.printStackTrace();
            }

            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        });
    }
}