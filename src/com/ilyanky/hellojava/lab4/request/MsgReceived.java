package com.ilyanky.hellojava.lab4.request;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.ilyanky.hellojava.lab4.Message;
import com.ilyanky.hellojava.lab4.RequestType;

import java.io.IOException;

/**
 * Created by ilyanky on 6/6/15.
 */
public class MsgReceived extends Request
{




    public MsgReceived()
    {
        super();
        super.requestType = RequestType.MSG_RECEIVED;
    }


    public MsgReceived(Message m)
    {
        this();
        super.dataNode.putPOJO("message", m);
    }


    public MsgReceived(byte[] bytes) throws IOException
    {
        super(bytes);
        super.requestType = RequestType.MSG_RECEIVED;
    }




    public Message getMessage() throws IOException
    {
        ObjectReader reader = new ObjectMapper().reader()
                .forType(Message.class);
        return reader.readValue(super.dataNode.get("message"));
    }
}
