package com.ilyanky.hellojava.lab4.request;

import com.ilyanky.hellojava.lab4.RequestType;

import java.io.IOException;

/**
 * Created by ilyanky on 6/6/15.
 */
public class AddFriendRequest extends Request
{



    public AddFriendRequest()
    {
        super();

        super.requestType = RequestType.ADD_FRIEND;
        super.dataNode.put("containsUser", false);
    }


    public AddFriendRequest(String login, String friendLogin)
    {
        this();
        super.dataNode.put("login", login);
        super.dataNode.put("friendLogin", friendLogin);
    }


    public AddFriendRequest(byte[] bytes) throws IOException
    {
        super(bytes);
        super.requestType = RequestType.ADD_FRIEND;
    }



    public void setUserContains(boolean b)
    {
        super.dataNode.put("containsUser", b);
    }




    public String getLogin()
    {
        return super.dataNode.get("login").asText();
    }


    public String getPassword()
    {
        return super.dataNode.get("password").asText();
    }


    public String getFriendLogin()
    {
        return super.dataNode.get("friendLogin").asText();
    }


    public boolean containsUser()
    {
        return super.dataNode.get("containsUser").asBoolean();
    }
}
