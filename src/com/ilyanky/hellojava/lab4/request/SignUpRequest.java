package com.ilyanky.hellojava.lab4.request;

import com.ilyanky.hellojava.lab4.RequestType;

import java.io.IOException;

/**
 * Created by ilyanky on 6/3/15.
 */
public class SignUpRequest extends Request
{



    public SignUpRequest()
    {
        super();

        super.requestType = RequestType.SIGN_UP;
        super.dataNode.put("containsUser", false);
    }


    public SignUpRequest(String login, String pswd)
    {
        this();
        super.dataNode.put("login", login);
        super.dataNode.put("password", pswd);
    }


    public SignUpRequest(byte[] bytes) throws IOException
    {
        super(bytes);
        super.requestType = RequestType.SIGN_UP;
    }



    public void setUserContains(boolean b)
    {
        super.dataNode.put("containsUser", b);
    }




    public String getLogin()
    {
        return super.dataNode.get("login").asText();
    }


    public String getPassword()
    {
        return super.dataNode.get("password").asText();
    }


    public boolean containsUser()
    {
        return super.dataNode.get("containsUser").asBoolean();
    }
}
