package com.ilyanky.hellojava.lab4.request;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ilyanky.hellojava.lab4.RequestType;

import java.io.IOException;
import java.nio.ByteBuffer;


/**
 * Created by ilyanky on 6/3/15.
 */
public abstract class Request
{
    protected RequestType    requestType = null;
    protected ObjectNode     dataNode = null;



    public Request()
    {
        dataNode = new ObjectMapper().createObjectNode();
    }


    public Request(byte[] bytes) throws IOException
    {
        dataNode = (ObjectNode)new ObjectMapper().readTree(bytes);
    }


    public Request(RequestType requestType)
    {
        this.requestType = requestType;
    }


    public RequestType getRequestType()
    {
        return requestType;
    }


    public boolean isSuccessful()
    {
        return dataNode.get("isSeccessgul").asBoolean();
    }


    public void setSuccessful(boolean successful)
    {
        dataNode.put("isSeccessgul", successful);
    }


    public byte[] toBytes() throws JsonProcessingException
    {
        byte[] bytes = new ObjectMapper().writeValueAsBytes(dataNode);
        ByteBuffer bb = ByteBuffer.allocate(4 + bytes.length);
        bb.putInt(requestType.getValue());
        bb.put(bytes);

        return bb.array();
    }


    public static Request getRequest(int id, byte[] bytes) throws IOException
    {
        if ( RequestType.LOG_IN.getValue() == id )
            return new LogInRequest(bytes);
        else if ( RequestType.SIGN_UP.getValue() == id )
            return new SignUpRequest(bytes);
        else if ( RequestType.ADD_FRIEND.getValue() == id )
            return new AddFriendRequest(bytes);
        else if ( RequestType.SEND_MSG.getValue() == id )
            return new SendMsgRequest(bytes);
        else if ( RequestType.MSG_RECEIVED.getValue() == id )
            return new MsgReceived(bytes);

        return null;
    }
}
