package com.ilyanky.hellojava.lab4.request;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ilyanky.hellojava.lab4.RequestType;
import com.ilyanky.hellojava.lab4.User;

import java.io.IOException;

/**
 * Created by ilyanky on 6/3/15.
 */
public class LogInRequest extends Request
{

    public LogInRequest()
    {
        super();

        super.requestType = RequestType.LOG_IN;
        super.dataNode.put("wrongPassword", false);
        super.dataNode.put("containsUser", false);
    }


    public LogInRequest(byte[] bytes) throws IOException
    {
        super(bytes);
        super.requestType = RequestType.LOG_IN;
    }


    public LogInRequest(String login, String pswd)
    {
        this();
        super.dataNode.put("login", login);
        super.dataNode.put("password", pswd);
    }


    public LogInRequest(ObjectNode node)
    {
        this();
        super.dataNode.set("user", node);
        super.dataNode.put("wrongPassword", false);
        super.dataNode.put("containsUser", true);
    }



    public void setAuthError(boolean b)
    {
        super.dataNode.put("wrongPassword", b);
    }

    public void setUserContains(boolean b)
    {
        super.dataNode.put("containsUser", b);
    }



    public String getLogin()
    {
        return super.dataNode.get("login").asText();
    }

    public String getPassword()
    {
        return super.dataNode.get("password").asText();
    }


    public User getUser() throws JsonProcessingException
    {
        return new User((ObjectNode)super.dataNode.get("user"));
    }


    public boolean authError()
    {
        return super.dataNode.get("wrongPassword").asBoolean();
    }

    public boolean containsUser()
    {
        return super.dataNode.get("containsUser").asBoolean();
    }
}
