package com.ilyanky.hellojava.lab4;

/**
 * Created by ilyanky on 5/19/15.
 */
public enum RequestType
{
    LOG_IN(0),
    SEND_MSG(1),
    SIGN_UP(2),
    ADD_FRIEND(3),
    UPDATE_USER(4),
    MSG_RECEIVED(5);

    private final int value;

    RequestType( int i )
    {
        this.value = i;
    }

    public int getValue()
    {
        return value;
    }
}
