package com.ilyanky.hellojava.lab4.dialogs;

import javax.swing.*;
import java.awt.*;

/**
 * Created by ilyanky on 6/6/15.
 */
public class InfoDialog extends JDialog
{
    private JTextArea infoArea = new JTextArea();

    public InfoDialog(JFrame owner)
    {
        super(owner, "Info", true);
        infoArea.setEditable(false);
        this.add(infoArea, BorderLayout.CENTER);
        this.setSize(200, 200);
    }


    public void setInfo(String info)
    {
        infoArea.setText(info);
    }
}
