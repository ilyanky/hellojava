package com.ilyanky.hellojava.lab1;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Created by ilyanky on 19.03.15.
 *
 * 17. Опишите функции для связывания объектов класса Stream с потоками
 * ввода вывода: входной поток преобразуется в Stream с использованием
 * передаваемой в качестве параметра функции чтения, а Stream может
 * писаться в выходной поток при помощи Collector-а.
 *
 */

public class StreamStudy
{


    public static String readLine(FileInputStream _fileInputStream)
    {
        ArrayList<Byte> string = new ArrayList<>();
        byte temp = 0;
        while ( temp != '\n' ) {
            try {
                int r = _fileInputStream.read();
                if ( r == -1 )
                    break;

                temp = (byte) r;
                if (temp != '\n')
                    string.add(temp);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ByteBuffer byteBuffer = ByteBuffer.allocate(string.size());
        string.forEach(byteBuffer::put);
        return new String(byteBuffer.array());
    }




    public static void main(String[] args) throws IOException
    {

        File file = new File("./src/com/ilyanky/hellojava/lab1/someText");

        Scanner fileSc = new Scanner(file);
        Stream<String> stream = StreamGenerator.stream(fileSc, () -> {
            String str = "1";
            while (!str.contains("my")) {
                if (!fileSc.hasNextLine())
                    break;
                str = fileSc.nextLine();
            }
            return str;
        });
        List<String> list = stream.filter( x -> x.contains("my") ) .collect(Collectors.toList());
        System.out.println("Stream: \n" + list + "\n");



        FileInputStream fileInputStream = new FileInputStream(file);
        Stream<String> stream1 = StreamGenerator.<String>stream(fileInputStream,
                () -> readLine(fileInputStream));
        List<String> list1 = stream1.filter( x -> x.contains("if") ).collect(Collectors.toList());
        System.out.println("Stream1: \n" + list1 + "\n");



        Stream<Integer> stream2 = StreamGenerator.stream(System.in, () -> {
            try {
                return System.in.read();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return 1;
        });
        System.out.println("Enter some sequence:");
        List<Integer> list2 = stream2.collect(Collectors.toList());
        System.out.println("Stream2: \n" + list2);
    }
}
