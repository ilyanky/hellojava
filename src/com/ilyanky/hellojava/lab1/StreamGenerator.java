package com.ilyanky.hellojava.lab1;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Spliterators;
import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Created by ilyanky on 22.03.2015.
 */
public final class StreamGenerator
{


    public static<T> Iterator<T> iterator(Iterator<T> _iter, Supplier<T> _supp)
    {
        return new Iterator<T>() {
            @Override
            public boolean hasNext() {
                return _iter.hasNext();
            }

            @Override
            public T next() {
                return _supp.get();
            }
        };
    }



    public static<T> Iterator<T> iterator(InputStream _istream, Supplier<T> _supp)
    {
        return new Iterator<T>() {
            private T m_data = null;
            private boolean isEnd = false;

            @Override
            public boolean hasNext() {
                if ( !isEnd )
                    m_data = _supp.get();
                try {
                    if ( _istream.available() == 0 && isEnd )
                        return false;
                    else if ( _istream.available() == 0 ) {
                        isEnd = true;
                        return true;
                    }
                    else
                        return true;
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
            }

            @Override
            public T next() {
                if ( m_data == null )
                    m_data = _supp.get();
                return m_data;
            }
        };
    }





    public static<T> Stream<T> stream(Iterator<T> _iter, Supplier<T> _supp)
    {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator(_iter, _supp), 0), false);
    }



    public static<T> Stream<T> stream(InputStream _istream, Supplier<T> _supp)
    {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator(_istream, _supp), 0), false);
    }
}
