package com.ilyanky.hellojava.lab2;

import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.LockSupport;

/**
 * Created by ilyanky on 4/18/15.
 */
public class Player
{
    private final LinkedBlockingQueue<Card> stack;
    private volatile boolean locked = true;
    private volatile boolean done = false;

    public boolean isDone()
    {
        return done;
    }

    public void setDone(boolean done)
    {
        this.done = done;
    }

    public Player(LinkedBlockingQueue<Card> _stack)
    {
        this.stack = _stack;
    }


    public Card peekCard()
    {
        return stack.peek();
    }


    public boolean isLocked()
    {
        return locked;
    }


    public void setLocked()
    {
        locked = true;
    }


    public void setUnlocked()
    {
        locked = false;
    }


    public LinkedList<Card> getStack()
    {
        return new LinkedList<>(stack);
    }


    public Card popCard()
    {
        try {
            return stack.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }


    public int stackSize()
    {
        return stack.size();
    }


    public void putCard(Card _card)
    {
        try {
            stack.put(_card);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
