package com.ilyanky.hellojava.lab2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Vector;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.LockSupport;

/**
 * Created by ilyanky on 4/24/15.
 */
public class War
{
    private final ArrayList<Player> players;
    private final ArrayList<Thread> threads;
    private final AtomicReference<Card> buff;
    private volatile boolean gameOver = false;



    public War(int nPlayers)
    {
        players = new ArrayList<>(nPlayers);
        buff = new AtomicReference<>();
        buff.set(null);
        threads = new ArrayList<>(nPlayers);

        int cards = 0;
        for ( int i = 0; i < nPlayers; ++i ) {
            players.add(i, new Player(new LinkedBlockingQueue<>(Card.getCardDeck().subList(cards, cards += 2))));
            ++cards;

            final int finalI = i;
            threads.add(i, new Thread(() -> {
                while ( !gameOver ) {
                    while ( true ) {
                        Card prev = buff.get();
                        Card next = players.get(finalI).peekCard();
                        if ( prev == null ) {
                            if ( buff.compareAndSet(prev, next) )
                                break;
                        } else {
                            if ( Card.compare(prev, next) < 0 ) {
                                if ( buff.compareAndSet(prev, next) )
                                    break;
                            } else
                                break;
                        }
                    }
                    players.get(finalI).setDone(true);
                    while ( players.get(finalI).isLocked() )
                        LockSupport.park();
                    players.get(finalI).setLocked();
                    players.get(finalI).setDone(false);
                    System.out.println("ends" + finalI);
                }
            }));
        }
    }


    public void game() throws InterruptedException
    {
        threads.forEach(java.lang.Thread::start);

//        Card[] cards = new Card[players.size()];
        while ( true ) {
            Card card;
            int player = 0;
            boolean done = false;
            while ( !done ) {
                card = buff.get();
                if ( players.get(0).isDone() ) {
                    if ( players.get(0).peekCard() == card )
                        player = 0;
                    done = true;
                } else
                    done = false;

                if ( players.get(1).isDone() ) {
                    if ( players.get(0).peekCard() == card )
                        player = 1;
                    done = true;
                } else
                    done = false;
            }

            System.out.println("Player" + player + " takes: ");
            Card[] cards = new Card[players.size()];
            for ( int i = 0 ; i < players.size(); ++i )
                cards[i] = players.get(i).popCard();

            final int finalPlayer = player;
            Arrays.stream(cards).forEach((x) -> {
                System.out.println(x.getValue() + " " + x.getSuit() + " ");
                players.get(finalPlayer).putCard(x);
            });


            try {
                players.stream().filter(x -> x.stackSize() == 0).findFirst().get();
                gameOver = true;
                for ( int i = 0; i < players.size(); ++i ) {
                    players.get(i).setUnlocked();
                    LockSupport.unpark(threads.get(i));
                }
                break;
            } catch ( NoSuchElementException e ) {
                for ( int i = 0; i < players.size(); ++i ) {
                    players.get(i).setUnlocked();
                    LockSupport.unpark(threads.get(i));
                    System.out.println("end");
                }
            }

        }

        for ( Thread t : threads )
            t.join();
    }

    public ArrayList<Player> getPlayers()
    {
        return players;
    }


}
