package com.ilyanky.hellojava.lab2;

/**
 * Created by ilyanky on 4/17/15.
 */
public enum Suit
{
    SPADES,
    HEART,
    DIAMONDS,
    CLUBS
}
