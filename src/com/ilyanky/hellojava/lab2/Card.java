package com.ilyanky.hellojava.lab2;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by ilyanky on 4/17/15.
 */
public class Card
{

    private Suit suit = null;
    private Value value = null;


    public Card(Value _value, Suit _suit)
    {
        this.value = _value;
        this.suit = _suit;
    }


    public Suit getSuit()
    {
        return suit;
    }

    public Value getValue()
    {
        return value;
    }



    @Override
    public String toString()
    {
        return "Value: " + value + " Suit: " + suit;
    }



    public static ArrayList<Card> getCardDeck()
    {
        final int DECK_SIZE = 36;

        ArrayList<Card> deck = new ArrayList<>(DECK_SIZE);
        for ( Suit s : Suit.values() )
            for ( Value v : Value.values() )
                deck.add(new Card(v, s));

        Collections.shuffle(deck);

        return deck;
    }


    public static int compare(Card _card1, Card _card2)
    {
        if ( _card1.getValue().getValue() < _card2.getValue().getValue() )
            return -1;
        else if ( _card1.getValue().getValue() > _card2.getValue().getValue() )
            return 1;
        else
            return 0;
    }
}
