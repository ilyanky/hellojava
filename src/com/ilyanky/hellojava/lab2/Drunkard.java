package com.ilyanky.hellojava.lab2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Vector;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.locks.LockSupport;

/**
 * Created by ilyanky on 4/18/15.
 */
public class Drunkard
{
    private final ArrayList<Player> players;
    private final ArrayList<Thread> threads;
    private final Vector<SynchronousQueue<Card>> buff;
    private volatile boolean gameOver = false;




    public Drunkard(int nPlayers)
    {
        players = new ArrayList<>(nPlayers);
        buff = new Vector<>(nPlayers);
        threads = new ArrayList<>(nPlayers);

        int cards = 0;
        for ( int i = 0; i < nPlayers; ++i ) {
            players.add(i, new Player(new LinkedBlockingQueue<>(Card.getCardDeck().subList(cards, cards += 2))));
            buff.add(i, new SynchronousQueue<>());
            ++cards;


            final int finalI = i;
            threads.add(i, new Thread(() -> {
                while ( !gameOver ) try {
                    buff.get(finalI).put(players.get(finalI).popCard());
                    while ( players.get(finalI).isLocked() )
                        LockSupport.park();
                    players.get(finalI).setLocked();
                    System.out.println("ends" + finalI);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }));
        }
    }


    public void game() throws InterruptedException
    {
        threads.forEach(java.lang.Thread::start);

        Card[] cards = new Card[players.size()];
        while ( true ) {
            for ( int i = 0; i < players.size(); ++i ) {
                Card temp = buff.get(i).take();
                cards[i] = new Card(temp.getValue(), temp.getSuit());
            }

            Card maxCard = cards[0];
            int maxPlayer = 0;
            for ( int i = 0; i < players.size(); ++i ) {
                if ( Card.compare(maxCard, cards[i]) < 0 ) {
                    maxCard = cards[i];
                    maxPlayer = i;
                }
            }

            System.out.println("Player" + maxPlayer + " takes: ");
            final int finalMaxPlayer = maxPlayer;
            Arrays.stream(cards).forEach((x) -> {
                System.out.println(x.getValue() + " " + x.getSuit() + " ");
                players.get(finalMaxPlayer).putCard(x);
            });


            try {
                players.stream().filter(x -> x.stackSize() == 0).findFirst().get();
                gameOver = true;
                for ( int i = 0; i < players.size(); ++i ) {
                    players.get(i).setUnlocked();
                    LockSupport.unpark(threads.get(i));
                }
                break;
            } catch ( NoSuchElementException e ) {
                for ( int i = 0; i < players.size(); ++i ) {
                    players.get(i).setUnlocked();
                    LockSupport.unpark(threads.get(i));
                    System.out.println("end");
                }
            }

        }

        for ( Thread t : threads )
            t.join();
    }

    public ArrayList<Player> getPlayers()
    {
        return players;
    }

}
