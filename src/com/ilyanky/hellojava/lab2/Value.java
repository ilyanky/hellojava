package com.ilyanky.hellojava.lab2;

/**
 * Created by ilyanky on 4/17/15.
 */
public enum Value
{
    SIX(6),
    SEVEN(7),
    EIGHT(8),
    NINE(9),
    TEN(10),
    JACK(11),
    QUEEN(12),
    KING(13),
    ACE(14);

    private final int value;

    Value(int i)
    {
        this.value = i;
    }


    public int getValue()
    {
        return value;
    }
}
